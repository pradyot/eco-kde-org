---
title: Eco Label
layout: eco-label
#menu:
#   main:
#     name: BE4FOSS
#     weight: 3
---
In 2020, the German Environment Agency (German: *Umweltbundesamt*) released the award criteria for eco-certifying desktop software with the Blue Angel ecolabel (German: *Blauer Engel*). From 2021-2022, the BE4FOSS project under KDE Eco worked to advance eco-certification for resource and energy-efficient software in FOSS communities. Obtaining the Blue Angel ecolabel occurs in 3 steps: (1) Measure, (2) Analyze, and (3) Certify.

Benefits of obtaining the Blue Angel ecolabel include:
- Recognition of reaching high standards for environmentally-friendly software design,
- Differentiating free software from the alternatives,
- Increasing the appeal of adoption for consumers, and
- Promoting transparency in the ecological footprint of software.

Learn more in the KDE Eco handbook "[Applying The Blue Angel Criteria To Free Software](https://eco.kde.org/handbook/)".